<?php

	namespace Core\Controller;

	class Controller{

		protected $viewPath;
		protected $template;
		protected $titre='PowerBall';
		protected $description='Site officiel de la Powerball personalisable';
		protected $indexation='';

		protected function render($view, $variables = []){
			$titre=explode('-',$_SERVER['REQUEST_URI']);
			ob_start();
			extract($variables);
			require ($this->viewPath . str_replace('.','/',$view) . '.php');
			$content = ob_get_clean();
			require ($this->viewPath. 'templates/' . $this->template . '.php');
		}

		protected static function forbidden(){
			header("HTTP/1.0 403 Forbidden");
			header("Location: /users-login");
			die;
		}

		protected function loadModel($model_name){
			$this->$model_name = App::getInstance()->getTable($model_name);
		}
	}