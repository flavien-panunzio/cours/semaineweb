<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="author" content="Panunzio Flavien"/>
		<meta name="copyright" content="© 2018 Flavien Panunzio"/>
		<title>PowerBall</title>
		<meta name="description" content="<?=$this->description?>"/>
		<meta name="keywords" content="sport, ball, powerball, power, competition, musculation">
		<link rel="icon" href="/images/favicon.png" type="image/png"/>

		<!-- COULEUR BAR URL IPHONE ANDROID WINDOWS-PHONE -->
		<meta name="apple-mobile-web-app-status-bar-style" content="232524"/>
		<meta name="theme-color" content="232524"/>
		<meta name="msapplication-navbutton-color" content="232524">
		
		<!-- CSS -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<link rel="stylesheet" href="/include/fullPage/fullpage.min.css">
		<link rel="stylesheet" type="text/css" href="/style/default/css/style.css">
	</head>
	<body>
		<div class="loading flex-center">
			<img id="load" src="/images/loading1.gif">
			<div class="none">
				<img src="/images/loading2.gif">
				<img src="/images/loading3.gif">
			</div>
		</div>
		<header id="header-landing">
			<div class="mobile menu">
				<div class="flex-center ancre gif" data-ancre="concept">
					<img>
					<p class="no-padding">Concept</p>
				</div>
				<div class="flex-center ancre gif" data-ancre="details">
					<img>
					<p class="no-padding">Détails</p>
				</div>
				<div class="flex-center ancre gif" data-ancre="personnalisation">
					<img>
					<p class="no-padding">Personnalisation</p>
				</div>
				<div class="flex-center ancre gif" data-ancre="contact">
					<img>
					<p class="no-padding">Contact</p>
				</div>
			</div>
			<nav>
				<div class="hamburger">
					<?php include(ROOT.'/public/images/general/menu.svg'); ?>
					<?php include(ROOT.'/public/images/general/croix.svg'); ?>
				</div>
				<div class="nav-items">
					<div class="flex-center ancre gif" data-ancre="concept">
						<img>
						<p class="no-padding">Concept</p>
					</div>
					<div class="flex-center ancre gif" data-ancre="details">
						<img>
						<p class="no-padding">Détails</p>
					</div>
				</div>
				<div class="brand ancre" data-ancre="index">
					<img>
				</div>
				<div class="nav-items">
					<div class="flex-center ancre gif" data-ancre="personnalisation">
						<img>
						<p class="no-padding">Personnalisation</p>
					</div>
					<div class="flex-center ancre gif" data-ancre="contact">
						<img>
						<p class="no-padding">Contact</p>
					</div>
				</div>
			</nav>
		</header>
		
		<?=$content;?>

		<!-- JAVASCRIPT -->
		<script src="/include/jquery.min.js"></script>
		<script src="/js/script.js"></script>
	</body>
</html>