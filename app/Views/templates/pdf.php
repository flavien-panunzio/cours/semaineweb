<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="author" content="Panunzio Flavien"/>
		<meta name="copyright" content="© 2018 Flavien Panunzio"/>
		<title>PowerBall</title>
		<link rel="icon" href="/images/favicon.png" type="image/png"/>

		<!-- CSS -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<link rel="stylesheet" href="/include/fullPage/fullpage.min.css">
		<link rel="stylesheet" type="text/css" href="/style/pdf/css/style.css">
	</head>
	<body>		
		<?=$content;?>
		<script src="/include/jquery.min.js"></script>
		<script src="/include/jquery.cookie.js"></script>
		<script type="text/javascript">
			window.print();
			input=$.cookie("input").split(',');
			console.log($.cookie("input"));
			$('#name').text(input[5]+' '+input[6]);
			$('#adress').text(input[7]);
			$('#tel').text(input[8]);
			$('#mail').text(input[9]);
			$('#haut').text(input[0]);
			$('#bas').text(input[3]);
			$('#bille').text(input[2]);
			$('#bandeau').text(input[1]);
			$('#prenom').text(input[4]);
		</script>
	</body>
</html>