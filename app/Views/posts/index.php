<div id="fullpage" class="landing">
	<div class="section" id="section1">
		<div class="container">
			<div class="flex-center">
				<img src="/images/landing.png">
				<h1 class="no-padding">POWERBALL</h1>
				<div>
					<span class="cyan">GIFT</span> &nbsp; <span class="gris">EDITION</span>
				</div>
			</div>
		</div>
	</div>
	<div class="section" id="section2">
		<div class="container">
			<h2 class="titre" data-title="CONCEPT">CONCEPT</h2><i class="fas fa-volume-up soundi" data-sound="soundConcept"></i>
			<div>
				<p>La Powerball, est un gadget de musculation simple d'utilisation et ludique. L'attraction que génère la balle vous permeet de muscler vos avant-bras. Vous pouvez retrouver ci-contre une vidéo de démonstration.</p>
				<iframe width="560" height="315" src="https://www.youtube.com/embed/6zOcSOcX4Nw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
	</div>
	<div class="section" id="section3">
		<div class="container">
			<h2 class="titre" data-title="DETAILS">DETAILS</h2>
			<div>
				<p>La partie du haut peut être également équipée d'un écran, vous donnant la possibilité de visualiser votre performance</p>
				<p>Cette bande en silicone permet d'avoir une très bonne adhésion contre la paume lors de l'utilisation.</p>
				<p>Le socle renferme le mécanisme, il peut également être de différentes couleurs.</p>
			</div>
		</div>
	</div>
	<div class="section" id="section4">
		<div class="container">
			<h2 class="mobile">PERSONNALISE</h2>
			<div class="grille">
				<div class="vue">
					<img class="img1" src="/images/choix/1.png">
					<img class="img2" src="/images/choix/2_cyan.png">
					<div class="flex-center">
						<?php include(ROOT.'/public/images/choix/3_blanc_vide.svg'); ?>
						<?php include(ROOT.'/public/images/choix/3_noir_vide.svg'); ?>
					</div>
					<img class="img4" src="/images/choix/4_jaune.png">
					<img class="img5" src="/images/choix/5.png">
					<img class="img6" src="/images/choix/6_cyan.png">
				</div>
				<div class="choix">
					<h2>PERSONNALISE</h2>
					<div class="grid">
						<div>
							<h3><span>1.</span> Choisis ta couleur</h3>
							<img class="preview" src="/images/choix/4_jaune.png">
							<form>
								<select id="object">
									<option value="2">Boitier haut</option>
									<option value="4">Bille</option>
									<option value="6">Boitier bas</option>
								</select>
								<select id="couleur">
									<option value="cyan">Cyan</option>
									<option value="jaune">Jaune</option>
									<option value="rouge">Rouge</option>
									<option value="vert">Vert</option>
									<option value="violet">Violet</option>
								</select>
								<input id="inpthaut" type="hidden" name="haut">
								<input id="inptbas" type="hidden" name="bas">
								<input id="inptbille" type="hidden" name="bille">
							<form>
						</div>
						<div>
							<h3><span>2.</span> Choisis ton bandeau</h3>
							<div class="prenom">
								<?php include(ROOT.'/public/images/choix/3_blanc_vide.svg'); ?>
								<?php include(ROOT.'/public/images/choix/3_noir_vide.svg'); ?>
							</div>
							<div class="flex">
								<form>
									<input type="hidden" name="couleurbandeau">
									<select id="bandeau">
										<option value="blanc">Blanc</option>
										<option value="noir">Noir</option>
									</select>
									<input type="text" name="nom" id="prenom" placeholder="Prénom...">
								</form>
							</div>
						</div>
						<div class="commander">
							<h3><span>3.</span> Offre lui la Powerball</h3>
							<div id="preco" class="button flex-center">PRÉCOMMANDER</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="section" id="section5">
		<div class="container">
			<h2 class="titre" data-title="FAQ">FAQ</h2><i class="fas fa-volume-up soundi" data-sound="soundFAQ"></i>
			<div class="flex-center">
				<ul>
					<li>Peut-on lancer la PowerBall sans la ficelle ?</li>
					<label>Oui, tout à fait, avec un peu d'entrainement, vous pouvez la lancer à la main.</label>
					<li>Quel est le record du monde actuel ?</li>
					<label>Le record du monde de vitesse est détenu par Serge Datan et il est de 22 573 tours par minutes.</label>
					<li>Comment changer les piles ?</li>
					<label>Pas besoin de changer la batterie, elle se recharge toute seule avec la rotation de la bille.</label>
					<li>J'ai fait tomber ma PowerBall et une partie c'est cassée, comment dois-je faire ?</li>
					<label>Touts nos produits sont garantis pendant 5 ans, vous pouvez <a href="#contact">nous contacter</a> pour l'envoi d'une pièce de rechange.</label>
				</ul>
			</div>
		</div>
	</div>
	<div class="section" id="section6">
		<div class="container">
			<h2 class="titre" data-title="CONTACT">CONTACT</h2>
			<div class="flex-center">
				<form class="flex-center">
					<div>
						<div class="col-5">
							<input type="text" name="nom" placeholder="Nom">
						</div>
						<div class="col-5">
							<input type="text" name="prenom" placeholder="Prénom">
						</div>
					</div>
					<div class="row flex-center">
						<input type="email" name="mail" placeholder="Mail">
						<textarea rows="7" placeholder="Votre message..."></textarea>
					</div>
				</form>
				<button id="message" class="submit">Envoyer</button>
			</div>
		</div>
		<div class="reseaux">
			<div class="flex-center"><a href="http://www.facebook.com"><i class="fab fa-facebook-f"></i></a></div>
			<div class="flex-center"><a href="http://www.instagram.com"><i class="fab fa-instagram"></i></a></div>
			<div class="flex-center"><a href="http://www.twitter.com"><i class="fab fa-twitter"></i></a></div>
		</div>
		<div class="bandeau">
			<p>Groupe POWERBALL &copy tous droits réservés</p>
			<a href="#">Mentions légales</a>
		</div>
	</div>
</div>

<audio autoplay="false" id="soundFAQ" src="/include/sound/FAQ.m4a"></audio>
<audio autoplay="false" id="soundConcept" src="/include/sound/Concept.m4a"></audio>

<div class="modal flex-center">
	<form class="flex-center">
		<input type="hidden" name="modalHaut" id="modalHaut">
		<input type="hidden" name="modalBandeau" id="modalBandeau">
		<input type="hidden" name="modalBille" id="modalBille">
		<input type="hidden" name="modalBas" id="modalBas">
		<input type="hidden" name="modalPrenom" id="modalPrenom">
		<input type="text" name="nom" placeholder="Nom">
		<input type="text" name="prenom" placeholder="Prénom">
		<input type="text" name="adresse" placeholder="Adresse">
		<input type="tel" name="number" placeholder="Téléphone">
		<input type="mail" name="mail" placeholder="Mail">
	</form>
	<div>
		<button id="fermer">Fermer</button>
		<button id="envoi">Envoyer</button>
	</div>
</div>