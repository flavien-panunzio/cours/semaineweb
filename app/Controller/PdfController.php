<?php 
	namespace App\Controller;
	use Core\Controller\Controller;
	use Core\Table\Table;

	class PdfController extends AppController{

		public function __construct(){
			parent::__construct();
		}

		public function index(){	
			$this->render('posts.pdf');
		}
	}