<?php

namespace App\Controller;

use Core\Controller\Controller;

class ErrorsController extends AppController{

	public function __construct(){
		parent::__construct();
	}

	public function error_404(){
		$indexation='<meta name="robots" content="noindex">';
		$this->render('errors.404',compact('indexation'));
	}
}