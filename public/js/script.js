//target _blank ----------------------------------------------------------------
	$("a[href^='http://'], a[href^='https://']").attr("target","_blank");

//fullpage ---------------------------------------------------------------------
$.getScript('/include/fullPage/fullpage.min.js', function(){
	new fullpage('#fullpage', {
		anchors: ['index', 'concept', 'details', 'personnalisation', 'faq','contact'],
		slidesNavigation: true,
		navigation: true,
		navigationPosition: 'right',
		licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE'
	});
	loading();
});

//son --------------------------------------------------------------------------
	$('audio').trigger("pause");
	$('.soundi').click(function(event) {
		var sound = $(this).data('sound');
		$('#'+sound).trigger("play");
	});

//ancres -----------------------------------------------------------------------
	$('.ancre').click(function(event) {
		location.hash=$(this).data('ancre')
	});


//MODAL -----------------------------------------------------------------------
	$('.modal #fermer').click(function(event) {
		$('.modal').removeClass('active');
	});
	$('#preco').click(function(event) {
		$('.modal').addClass('active');
		$('#modalBandeau').val( $('#bandeau').val() );
		$('#modalPrenom').val( $('#prenom').val() );
		$('#modalBas').val( $('#inptbas').val() );
		$('#modalHaut').val( $('#inpthaut').val() );
		$('#modalBille').val( $('#inptbille').val() );
	});
	$('#envoi').click(function(event) {
		var input = new Array();
		$('.modal input').each(function(){
			input.push($(this).val());
		});
		$.removeCookie("input");
		$.cookie("input",input);
		console.log($.cookie("input").split(','))
		window.open('/pdf-index');
	});

//SELECTION --------------------------------------------------------------------
function displayVals() {
	var object = $( "#object" ).val();
	var couleur = $( "#couleur" ).val();
	var bandeau = $( "#bandeau" ).val();
	if (bandeau=='blanc') {
		$(".3_noir_vide").css('display', 'none');
		$(".3_blanc_vide").css('display', 'initial');
		$('svg text').css('fill', 'black');
	}else{
		$(".3_blanc_vide").css('display', 'none');
		$(".3_noir_vide").css('display', 'initial');
		$('svg text').css('fill', 'white');
	}
	$('.preview').attr('src', '/images/choix/'+object+'_'+couleur+'.png');

	switch(object) {
		case '2':
			$('.img2').attr('src', '/images/choix/2_'+couleur+'.png');
			break;
		case '4':
			$('.img4').attr('src', '/images/choix/4_'+couleur+'.png');
			break;
		case '6':
			$('.img6').attr('src', '/images/choix/6_'+couleur+'.png');
			break;
	}
	Haut = $('.img2').attr('src').replace('.png', '').split('_');
	Bas = $('.img6').attr('src').replace('.png', '').split('_');
	Bille = $('.img4').attr('src').replace('.png', '').split('_');

	$('#inptbas').val(Bas[1]);
	$('#inpthaut').val(Haut[1]);
	$('#inptbille').val(Bille[1]);
}
$("#couleur, #bandeau").focus( displayVals );
$("#couleur, #bandeau").change( displayVals );
displayVals();

//MODIF NAV SCROLL -------------------------------------------------------------

$(window).on('hashchange', function(){
	changementAncre(window.location.hash);
});
changementAncre(window.location.hash);
function changementAncre(ancre){
	if(ancre==''){ancre='#index'}
	switch(ancre) {
		case '#index' || '':
			coul1="#1fd4bd";
			logo="cyan";
			$('header').css({'background': 'linear-gradient( rgb(241,241,241) 0%, rgb(241,241,241) 50%,	rgba(0,0,0,0) 100%)','color': '#171717'});
		break;
		case '#concept':
			coul1="#d20646";
			logo="rouge";
			$('header').css({'background': 'linear-gradient( rgb(241,241,241) 0%, rgb(241,241,241) 50%,	rgba(0,0,0,0) 100%)','color': '#171717'});
		break;
		case '#details':
			coul1="#d78000";
			logo="orange";
			$('header').css({'background': 'linear-gradient( rgb(241,241,241) 0%, rgb(241,241,241) 50%,	rgba(0,0,0,0) 100%)','color': '#171717'});
		break;
		case '#personnalisation':
			coul1='#1fd4bd';
			logo="cyan";
			$('header').css({'background': 'linear-gradient( #1e1e1e 0%, #1e1e1e 100%)','color': 'white'});
		break;
		case '#faq':
			coul1="#5e1abd";
			logo="violet";
			$('header').css({'background': 'linear-gradient( rgb(241,241,241) 0%, rgb(241,241,241) 50%,	rgba(0,0,0,0) 100%)','color': '#171717'});
		break;
		case '#contact':
			coul1="#15c449";
			logo="vert";
			$('header').css({'background': 'linear-gradient( rgb(241,241,241) 0%, rgb(241,241,241) 50%,	rgba(0,0,0,0) 100%)','color': '#171717'});
		break;
	}
	
	$('.st0').attr('fill', coul1);
	$('.gif img').attr('src', '/images/menu/shape_'+logo+'.png');
	$('.brand img').attr('src', '/images/menu/logo_'+logo+'.png');
	$('#fp-nav ul li a span, .fp-slidesNav ul li a span').attr('style', 'background:'+coul1);
}

//Menu mobile -------------------------------------------------------------------------------------
$('.hamburger').click(function(event) {
	$('.menu, .hamburger').toggleClass('active');
});
$('.ancre').click(function(event) {
	$('.menu, .hamburger').removeClass('active');
});
$(window).scroll(function(event) {
	$('.menu, .hamburger').removeClass('active');
});


//TEXTE input  ------------------------------------------------------------------------------------
	$('input').keyup(function(event) {
		$('textPath').text($(this).val());
	});

	$("#message").click(function(event) {
		alert('Message envoyé')
	});


//Annimation loading
function loading(){
	var gif1 = 2000;
	var gif2 = 2000;
	var gif3 = 2000;

	window.setTimeout(function () {
		$('#load').attr("src", '/images/loading2.gif');
		window.setTimeout(function () {
			$('#load').attr("src", '/images/loading3.gif');
			$(document).ready(function(){
				window.setTimeout(function () {
					$('.loading').css("display", 'none');
					$('.loading').css('opacity', 0);
					window.setTimeout(function () {
						$('.loading').css('z-index', -1);
					}, 500);
				}, gif3);
			});
		}, gif2);
	}, gif1);
}