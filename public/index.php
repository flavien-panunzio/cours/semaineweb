<?php
	define('ROOT', dirname(__DIR__));
	require ROOT . '/app/App.php';
	App::load();

	$URL=explode('/',$_SERVER['REQUEST_URI']);
	if (count($URL)>2)
		$_GET['id']=$URL[2];

	if ($URL[1]!='')
		$p=$URL[1];
	else 
		$p='posts-index';

	if ($URL[1]=='index.php')
		$p='posts-index';
	$p = explode('-', $p);

	$array = ["app", "posts", 'pdf', "users", "errors"];
	$array2 = ["app", "posts", "categories"];

	if ($p[0] == 'admin'){
		if (isset($p[1])) {
			if(!in_array($p[1], $array2)){
				header('location: /errors-error_404');
				die;
			}
			$controller = '\App\Controller\Admin\\' . ucfirst($p[1]) . 'Controller';
			if (isset($p[2])) {
				$action = $p[2];
			}
			else{
				header('location: /errors-error_404');
				die;
			}
		}
		else{
			header('location: /errors-error_404');
			die;
		}
	}
	else{
		if(!in_array($p[0], $array)){
			header('location: /errors-error_404');
			die;
		}
		$controller = '\App\Controller\\' . ucfirst($p[0]) . 'Controller';
		if (isset($p[1])) {
			$action = $p[1];
		}
		else{
			header('location: /errors-error_404');
			die;
		}
	}
	$controller = new $controller();
	if(method_exists($controller, $action)){
		$controller->$action();
	}
	else{
		header('location: /errors-error_404');
		die;
	}